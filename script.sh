#!/bin/bash

echo 'Extract busybox.tar and checksum.txt';
tar.exe -a -x -f package.tar
echo 'Creating new checksum2.txt';
certutil.exe -hashfile "busybox.tar" SHA256 >> checksum2.txt
echo 'Comparing checksum.txt and checksum2.txt to make sure there was no tampering';
fc.exe checksum.txt checksum2.txt > checksum_results.txt

# Running the script2
#./script2.sh

function foo() {
    echo 'bar';
}