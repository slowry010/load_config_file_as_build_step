package com.scott.load_config_file_as_build_step

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import com.scott.load_config_file_as_build_step.Hello

@SpringBootApplication
class LoadConfigFileAsBuildStepApplication

fun main(args: Array<String>) {
	println("Hello from main")
	runApplication<LoadConfigFileAsBuildStepApplication>(*args)
}
