package com.scott.load_config_file_as_build_step

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class HelloTest {
    val hello = Hello()

    @Test
    fun testingHelloClass(){
        assertThat(hello.printHelloWorld()).isEqualTo("This is Hello World")
    }
//    TODO take in a variable from a config and use that

}